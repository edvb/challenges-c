#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[]) {
	char *str;
	char *revstr;
	int len;

	if (argc < 2) {
		printf("Usage: revstr STRINGs ...\n");
		return 1;
	}

	do {
		revstr = malloc(sizeof(char));
		str = argv[argc-1];
		len = strlen(str);
		for (int i = 0, j = len-1; j >= 0; i++, j--)
			revstr[i] = str[j];
		printf("%s ", revstr);
	} while (--argc-1);

	printf("\n");
	free(revstr);
	return 0;
}
