#include <stdio.h>

int
main(int argc, char *argv[]) {
	int c, wc = 0;

	while ((c = getchar()) != EOF)
		if (c == ' ' || c == '\n' || c == '\t')
			wc++;

	printf("%d\n", wc);

	return 0;
}
