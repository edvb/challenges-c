#include <stdio.h>
#include <stdlib.h>

long factorial(long num) {
	if (num != 1) /* still has more numbers to multiply */
		return num * factorial(num - 1);
	else /* reached end */
		return num;
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("Usage: factorial NUMBER\n");
		return 1;
	}

	printf("%ld\n", factorial(atoi(argv[1])));
	return 0;
}

