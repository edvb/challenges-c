#include <stdio.h>
#include <stdlib.h>

#include "lua5.3/lua.h"
#include "lua5.3/lualib.h"
#include "lua5.3/lauxlib.h"

int
main(int argc, char *argv[]) {
	if (argc < 2) {
		printf("Usage: two-lang <message>\n");
		return 1;
	}

	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	lua_pushstring(L, argv[1]);
	lua_setglobal(L, "msg");
	if (luaL_dofile(L, "two-lang.lua")) {
		fprintf(stderr, "%s\n", lua_tostring(L, -1));
		lua_pop(L, -1);
		exit(EXIT_FAILURE);
	}
	lua_close(L);

	return 0;
}
