#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[]) {
	int arg;
	long o, n;
	int i;

	if (argc != 2) {
		printf("Usage: fibonacci NUMBER\n");
		return 1;
	}

	arg = atoi(argv[1]);
	o = n = 1;

	printf("%ld \n", o);
	if (arg > 1) 
		printf("%ld \n", n);
	for (i = 2; i < arg; i++) {
		n = n + o;
		o = n - o;
		printf("%ld \n", n);
	}

	return 0;
}
