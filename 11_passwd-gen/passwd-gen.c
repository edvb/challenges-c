#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int
main(int argc, char *argv[]) {
	char chars[] = "abcdefghijklmnopqrstuvwxyz0123456789;',.?:+-";
	char *pass = malloc(sizeof(char));
	int i;
	srand(time(NULL));

	if (argc != 2) {
		printf("Usage: passwd-gen LENGTH\n");
		return 1;
	}

	for (i = 0; i < atoi(argv[1]); i++)
		pass[i] = chars[rand() % 44];

	printf(pass);
	printf("\n");

	free(pass);
	return 0;
}
