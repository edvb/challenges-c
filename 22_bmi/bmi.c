#include <stdio.h>

int
main(void) {
	float h, w, bmi;

	printf("What is your height in inches? ");
	scanf("%f", &h);

	printf("What is your weight in pounds? ");
	scanf("%f", &w);

	bmi = (180/(h*h))*703;

	printf("Your BMI is approximately %f \n", bmi);

	return 0;
}
