### Change the varibles below for your system

# includes and libs
INCS = -Iinclude
LIBS =

# flags
CFLAGS = -std=c99 -pedantic -Wall ${INCS}
LDFLAGS = ${LIBS}

# compiler and linker
CC = gcc

### Makefile

EXE = $(basename $(notdir $(shell pwd))).out
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)

all: options $(EXE)

options:
	@echo $(EXE) build options:
	@echo "CC      = $(CC)"
	@echo "CFLAGS  = $(CFLAGS)"
	@echo "LDFLAGS = $(LDFLAGS)"

$(OBJ):

.o:
	@echo LD $@
	@$(LD) -o $@ $< $(LDFLAGS)

.c.o:
	@echo CC $<
	@$(CC) -c -o $@ $< $(CFLAGS)

$(EXE): $(OBJ)
	@echo CC -o $@
	@$(CC) -o $@ $(OBJ) $(LDFLAGS)

run: all
	./$(EXE)

clean:
	@echo -n cleaning...
	@rm -f $(OBJ) $(EXE)
	@echo \ done

.PHONY: all options run clean
