#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[]) {
	int namenum[2]; /* each name as a single number */

	if (argc != 3) {
		printf("Usage: lovecalc NAME1 NAME2\n");
		return 1;
	}

	for (int i = 1; i <= 2; i++)
		for (int j = 0; j < strlen(argv[i]); j++)
			namenum[i] += argv[i][j];

	printf("%d\n", (namenum[1] + namenum[2]) % 100);

	return 0;
}
