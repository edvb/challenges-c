#include <stdio.h>
#include <time.h>
#include <unistd.h>

char *numbers[11][7] = {
	{ "  #####  ",
	  " ##   ## ",
	  "##     ##",
	  "##     ##",
	  "##     ##",
	  " ##   ## ",
	  "  #####  "
	},
	{ "    ##   ",
	  "  ####   ",
	  "    ##   ",
	  "    ##   ",
	  "    ##   ",
	  "    ##   ",
	  "  ###### "
	},
	{ " ####### ",
	  "##     ##",
	  "       ##",
	  " ####### ",
	  "##       ",
	  "##       ",
	  "#########"
	},
	{ " ####### ",
	  "##     ##",
	  "       ##",
	  " ####### ",
	  "       ##",
	  "##     ##",
	  " ####### "
	},
	{ "##       ",
	  "##    ## ",
	  "##    ## ",
	  "##    ## ",
	  "#########",
	  "      ## ",
	  "      ## "
	},
	{ "######## ",
	  "##       ",
	  "##       ",
	  "#######  ",
	  "      ## ",
	  "##    ## ",
	  " ######  "
	},
	{ " ####### ",
	  "##     ##",
	  "##       ",
	  "######## ",
	  "##     ##",
	  "##     ##",
	  " ####### "
	},
	{ "######## ",
	  "##    ## ",
	  "    ##   ",
	  "   ##    ",
	  "  ##     ",
	  "  ##     ",
	  "  ##     "
	},
	{ " ####### ",
	  "##     ##",
	  "##     ##",
	  " ####### ",
	  "##     ##",
	  "##     ##",
	  " ####### "
	},
	{ " ####### ",
	  "##     ##",
	  "##     ##",
	  " ########",
	  "       ##",
	  "##     ##",
	  " ####### "
	},
	{ "      ",
	  "  ##  ",
	  "  ##  ",
	  "      ",
	  "  ##  ",
	  "  ##  ",
	  "      "
	}
};

int
main(void) {
	time_t rawtime;
	struct tm *timeinfo;

	while (1) { /* keep updating clock */
		time(&rawtime); /* get time */
		timeinfo = localtime(&rawtime);
		int digits[5] = { /* format time */
			timeinfo->tm_hour / 10 % 10, timeinfo->tm_hour % 10, 10,
			timeinfo->tm_min  / 10 % 10, timeinfo->tm_min  % 10,
		};

		/* print time */
		for (int i = 0; i < 7; i++) {
			for (int num = 0; num < 5; num++) {
				printf(numbers[digits[num]][i]);
				printf(" ");
			}
			printf("\n");
		}

		sleep(1);

		/* reset cursor to draw next time */
		printf("\033[7A"); /* move up 7 characters */
		printf("\033[45D"); /* move left 45 characters */
	}

	return 0;
}
