#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[]) {
	float in, out;

	if (argc != 3) {
		printf("Usage: temp-conv <-f|--fahrenheit|-c|--celsius> <degrees>\n"
		       "Convert one temperature to another\n");
		return 1;
	}

	in = atoi(argv[2]);

	if (argv[1][1] == 'f' || strcmp(argv[1], "--fahrenheit") == 0) {
		out = (in-32)*5/9;
		printf("%.2f degrees celsius\n", out);
	} else if (argv[1][1] == 'c' || strcmp(argv[1], "--celsius") == 0) {
		out = in*9/5+32;
		printf("%.2f degrees fahrenheit\n", out);
	} else
		printf("Not a recognized input type\n"
		       "-f --fahrenheit -c and --celsius are only valid options\n");

	return 0;
}
