# challenges-c

A repo for storing each of our attempts of the challenges

Please look at our [coding style](https://gitlab.com/edvb/challenges-c/blob/master/coding_style.md) before writing any code

![prog-challenges](prog-challenges.png)

# License

GPL v3 License
